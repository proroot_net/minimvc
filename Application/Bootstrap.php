<?php

/**
 * Load the core class
 */

require_once APPPATH . 'Classes/Core/Core.php';

/**
 * Enable the Core autoLoader.
 */

spl_autoload_register ([
	'Core_Core',
	'AutoLoad'
]);

set_exception_handler ([
	'Core_Exception_Exception',
	'Handler'
]);

Core_Core::Init ();

Core_Route::Set ('home')
	->Defaults ([
		'Controller' => 'Welcome'
	])
	->Home ();

Core_Route::Set ('test')
	->Defaults ([
		'Controller' => 'Welcome',
		'Action'     => 'Index'
	]);

Core_Route::Set ('test_2')
	->Defaults ([
		'Controller' => 'Test',
		'Action'     => 'Index'
	]);

Core_Route::Set ('errorPage')
	->Defaults ([
		'Controller' => 'Error'
	])
	->Error ();