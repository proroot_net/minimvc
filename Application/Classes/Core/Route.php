<?php

class Core_Route
{
	public static $uDefaultAction = 'Index';

	/**
	 * @var array
	 */

	private static $_uRoutes = [];

	public static function Set ($uName)
	{
		if (isset (self::$_uRoutes[$uName]))
			throw new Core_Exception_Exception ('Данный маршрут: :uRoute существует', [
				':uRoute' => $uName
			]);

		return self::$_uRoutes[$uName] = new self ();
	}

	public static function All ()
	{
		return self::$_uRoutes;
	}

	public static function Get ($uName)
	{
		if ( ! isset (self::$_uRoutes[$uName]))
			throw new Core_Exception_Exception ('Не существует данный маршрут: :uRoute', [
				':uRoute' => $uName
			]);

		return self::$_uRoutes[$uName];
	}

	private $_uDefaults = [];

	public function __construct ()
	{

	}

	public function Defaults (array $uDefaults = null)
	{
		if ($uDefaults === null)
			return $this->_uDefaults;

		$this->_uDefaults = $uDefaults;

		return $this;
	}

	public function Home ()
	{
		$this->_uDefaults['Home'] = true;
	}

	public function Error ()
	{
		$this->_uDefaults['Error'] = true;
	}
}