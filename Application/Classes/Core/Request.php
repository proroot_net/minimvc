<?php

class Core_Request
{
	private static $uTrustedProxies = [
		'127.0.0.1',
		'localhost',
		'localhost.localdomain'
	];

	public static $uUserAgent      = '';

	public static $uClientIp       = '0.0.0.0';

	public static function Factory ()
	{
		$uMethod = (isset ($_SERVER['REQUEST_METHOD']))
			? $_SERVER['REQUEST_METHOD']
			: 'GET';

		// Проверка на безопасное подключение https
		if ((( ! empty ($_SERVER['HTTPS'])) && (filter_var ($_SERVER['HTTPS'], FILTER_VALIDATE_BOOLEAN)))
			|| ((isset ($_SERVER['HTTP_X_FORWARDED_PROTO'])) && ($_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https'))
				&& (in_array ($_SERVER['REMOTE_ADDR'], self::$uTrustedProxies)))
			$uSecure = true;

		// Получаем referrer
		if (isset ($_SERVER['HTTP_REFERER']))
			$uReferrer = $_SERVER['HTTP_REFERER'];

		// Получаем user agent
		if (isset ($_SERVER['HTTP_USER_AGENT']))
			self::$uUserAgent = $_SERVER['HTTP_USER_AGENT'];

		// Проверка на Ajax - запрос
		if ((isset ($_SERVER['HTTP_X_REQUESTED_WITH']))&& (strtolower ($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'))
			$uAjax = true;

		// Получаем IP - адрес пользователя
		if ((isset ($_SERVER['HTTP_X_FORWARDED_FOR'])) && (isset ($_SERVER['REMOTE_ADDR']))
			&& (in_array ($_SERVER['REMOTE_ADDR'], self::$uTrustedProxies)))
			// Определяем реальный IP - адрес
			self::$uClientIp = array_shift (explode (',', $_SERVER['HTTP_X_FORWARDED_FOR']));
		elseif ((isset ($_SERVER['HTTP_CLIENT_IP'])) && (isset ($_SERVER['REMOTE_ADDR']))
			&& (in_array ($_SERVER['REMOTE_ADDR'], self::$uTrustedProxies)))
			// Определяем реальный IP - адрес
			self::$uClientIp = array_shift (explode (',', $_SERVER['HTTP_CLIENT_IP']));
		elseif (isset ($_SERVER['REMOTE_ADDR']))
			self::$uClientIp = $_SERVER['REMOTE_ADDR'];

		$uRequest = new self ();

		if (isset ($_GET['uRoute']))
			$uRequest->ClientRoute ($_GET['uRoute']);

		if (isset ($uSecure))
			$uRequest->Secure ($uSecure);

		if (isset ($uMethod))
			$uRequest->Method ($uMethod);

		if (isset ($uReferrer))
			$uRequest->Referrer($uReferrer);

		if (isset ($uAjax))
			$uRequest->Ajax($uAjax);

		return $uRequest;
	}

	public static function Proccess (Core_Request $uRequest)
	{
		$uClientRoute = $uRequest->ClientRoute ();

		$uParams = [];

		foreach (Core_Route::All () as $uName => $uRoute)
		{
			$uRouteDefaults = $uRoute->Defaults ();

			if (isset ($uRouteDefaults['Home']))
			{
				unset ($uRouteDefaults['Home']);

				$uParams['uHomeParams'] = $uRouteDefaults;
			}

			if (isset ($uRouteDefaults['Error']))
			{
				unset ($uRouteDefaults['Error']);

				$uParams['uErrorParams'] = $uRouteDefaults;
			}

			if ($uClientRoute == $uName)
				$uParams['uClientParams'] = $uRouteDefaults;

			if (count ($uParams) == 2)
				break;
		}

		return $uParams;
	}

	private $_uController;

	private $_uAction;

	private $_uClientRoute = '';

	private $_uMethod      = 'GET';

	private $_uSecure      = false;

	private $_uReferrer    = '';

	private $_uAjax        = false;

	public function __construct ()
	{
		return $this;
	}

	public function Execute ()
	{
		$uProcessed = self::Proccess ($this);

		if (isset ($uProcessed['uClientParams']))
			$uParams = $uProcessed['uClientParams'];
		elseif ((isset ($uProcessed['uErrorParams'])) && ( ! empty ($this->ClientRoute ())))
			$uParams = $uProcessed['uErrorParams'];
		elseif (isset ($uProcessed['uHomeParams']))
			$uParams = $uProcessed['uHomeParams'];

		if ( ! isset ($uProcessed))
			throw new Core_Exception_Exception ('Не удалось определить параметры маршрута..');

		$this->Controller ($uParams['Controller']);

		$this->Action ((isset ($uParams['Action']))
			? $uParams['Action']
			: Core_Route::$uDefaultAction
		);

		$uNameController = 'Controller_' . $this->Controller ();

		if ( ! class_exists ($uNameController))
			throw new Core_Exception_Exception ('Не существует класс контроллера: :uController', [
				':uController' => $uNameController
			]);

		return new $uNameController ($this);
	}

	public function ClientRoute ($uClientRoute = null)
	{
		if ($uClientRoute === null)
			return $this->_uClientRoute;

		$this->_uClientRoute = (! empty ($uClientRoute))
			? strtolower ($uClientRoute)
			: '';

		return $this;
	}

	public function Secure ($uSecure = null)
	{
		if ($uSecure === null)
			return $this->_uSecure;

		$this->_uSecure = (bool) $uSecure;

		return $this;
	}

	public function Controller ($uController = null)
	{
		if ($uController === null)
			return $this->_uController;

		$this->_uController = (string) $uController;

		return $this;
	}

	public function Action ($uAction = null)
	{
		if ($uAction === null)
			return $this->_uAction;

		$this->_uAction = (string) $uAction;

		return $this;
	}

	public function Method ($uMethod = null)
	{
		if ($uMethod === null)
			return $this->_uMethod;

		$this->_uMethod = strtoupper ($uMethod);

		return $this;
	}

	public function Referrer ($uReferrer = null)
	{
		if ($uReferrer === null)
			return $this->_uReferrer;

		$this->_uReferrer = (string) $uReferrer;

		return $this;
	}

	public function Ajax ($uAjax = null)
	{
		if ($uAjax === null)
			return $this->_uAjax;

		$this->_uAjax = (bool) $uAjax;

		return $this;
	}
}