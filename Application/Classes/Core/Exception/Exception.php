<?php

class Core_Exception_Exception extends Exception
{
	public function __construct ($uMessage = null, array $uVariables = null, $uCode = 0, Exception $uPrevious = null)
	{
		if ($uVariables)
			$uMessage = strtr ($uMessage, $uVariables);

		parent::__construct ($uMessage, (int) $uCode, $uPrevious);
	}

	public static function Handler (Exception $uE)
	{
		if (DEBUG)
			echo sprintf ('%s [ %s ]: %s ~ %s [ %d ]', get_class($uE), $uE->getCode(), strip_tags($uE->getMessage()), $uE->getFile(), $uE->getLine());
	}
}